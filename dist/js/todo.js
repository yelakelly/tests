'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function generateID() {
    return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
}

var Todo = function () {
    function Todo(text) {
        var status = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

        _classCallCheck(this, Todo);

        this.text = text;
        this.status = status;
        this.id = '#' + generateID();
    }

    _createClass(Todo, [{
        key: 'setActive',
        value: function setActive() {
            this.status = true;
        }
    }, {
        key: 'setDisable',
        value: function setDisable() {
            this.status = false;
        }
    }, {
        key: 'render',
        value: function render() {
            return '<div class="todo-list__item is-' + (this.status ? 'active' : 'disable') + '" id="' + this.id + '">\n            <input class="default-checkbox" type="checkbox" id=\'' + (this.id + '__label') + '\' ' + (this.status ? 'checked' : '') + '>\n            <label for=\'' + (this.id + '__label') + '\'>\n                <span>' + this.text + '</span>\n                <span class="icon-tick-css"></span>\n            </label>\n            <span class="todo-list__remove-btn icon-close">&times;</span>\n        </div>';
        }
    }]);

    return Todo;
}();

var TodoList = function () {
    function TodoList(selector) {
        _classCallCheck(this, TodoList);

        this.list = {};
        this.selector = selector;
        this.selector.$todo = this;

        this.setEvents();
    }

    _createClass(TodoList, [{
        key: 'add',
        value: function add(el) {
            this.list[el.id] = el;
            this.render();
        }
    }, {
        key: 'getTodo',
        value: function getTodo(id) {
            return this.list[id];
        }
    }, {
        key: 'remove',
        value: function remove(id) {
            delete this.list[id];
            this.render();
        }
    }, {
        key: 'setEvents',
        value: function setEvents() {
            var _this = this;

            this.selector.addEventListener('click', function (e) {
                var el = e.target;

                if (el.classList.contains('todo-button')) {
                    _this._createTodo();
                }

                if (el.classList.contains('todo-list__remove-btn')) {
                    var todoID = el.closest('.todo-list__item').getAttribute('id');
                    _this.remove(todoID);
                }
            });

            this.selector.addEventListener('keyup', function (e) {
                if (e.target.classList.contains('todo-input') && e.keyCode == 13) {
                    _this._createTodo();
                }
            });

            this.selector.addEventListener('change', function (e) {
                var target = e.target;
                if (target.classList.contains('default-checkbox')) {
                    var todoID = target.getAttribute('id').replace('__label', '');
                    var todo = _this.getTodo(todoID);

                    target.checked ? todo.setActive() : todo.setDisable();
                    _this.render();
                }
            }, {
                'capture': true
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var markup = '';
            var values = Object.keys(this.list).map(function (e) {
                return _this2.list[e];
            });

            values.forEach(function (el) {
                return markup += el.render();
            });

            this.selector.innerHTML = '\n            <input type="text" class="form-input todo-input" name="todo-input">\n            <button class="todo-button btn">\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C</button>\n            <div class="todo-list">\n                ' + markup + '\n            </div>\n        ';
        }
    }, {
        key: 'validateInput',
        value: function validateInput(el) {
            var valid = false;
            el.classList.remove('is-error');

            if (el.value.trim()) {
                valid = true;
            }

            el.classList.add('is-error');
            return valid;
        }
    }, {
        key: '_createTodo',
        value: function _createTodo() {
            var input = document.querySelector('input[name="todo-input"]');

            if (this.validateInput(input)) {
                this.add(new Todo(input.value));
            }
        }
    }]);

    return TodoList;
}();

var react = new Todo('React');
var nodejs = new Todo('Node.js');

var todoList = new TodoList(document.querySelector('[todo-list]'));
todoList.add(react);
todoList.add(nodejs);