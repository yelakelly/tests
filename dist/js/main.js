'use strict';

document.addEventListener("DOMContentLoaded", function (event) {
    var scroll = new SmoothScroll('a[href*="#"]');
    var moreButtons = document.querySelectorAll('.more-button');

    for (var i = 0; i < moreButtons.length; i++) {
        var btn = moreButtons[i];

        btn.addEventListener('click', function (e) {
            var target = document.querySelector(this.getAttribute('data-target'));

            target.classList.toggle('is-active');
            this.classList.toggle('is-open');

            var buttonInner = this.querySelector('span');

            if (this.classList.contains('is-open')) {
                buttonInner.innerHTML = buttonInner.getAttribute('data-hide-text');
            } else {
                buttonInner.innerHTML = buttonInner.getAttribute('data-show-text');
            }

            e.preventDefault();
        });
    }

    var classIndex = 0;
    var lastClass = 'has-tooltip--top';
    var classList = ['has-tooltip--right', 'has-tooltip--bottom', 'has-tooltip--left', 'has-tooltip--top'];

    document.querySelector('.has-tooltip').addEventListener('click', function () {
        this.classList.remove(lastClass);
        this.classList.add(classList[classIndex]);
        lastClass = classList[classIndex];
        classIndex = classIndex == classList.length - 1 ? 0 : classIndex + 1;
    });

    var copyToClipboard = function copyToClipboard() {
        var el = document.createElement('textarea');
        el.value = Array.from(document.querySelectorAll('[to-clipboard]')).reduce(function (prev, curr, index) {
            if (index == 1) {
                prev = prev.getAttribute('to-clipboard');
            }
            return prev + '\n' + curr.getAttribute('to-clipboard');
        });
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };

    document.querySelector('.about__buffer-control').addEventListener('click', function () {
        copyToClipboard();
    });

    document.querySelector('.js-nav-toggle').addEventListener('click', function () {
        this.classList.toggle('is-active');
        document.querySelector('body').classList.toggle('has-open-menu');
    });

    Array.from(document.querySelectorAll('.primary-nav__link')).forEach(function (el) {
        el.addEventListener('click', function () {
            document.querySelector('body').classList.remove('has-open-menu');
            document.querySelector('.js-nav-toggle').classList.remove('is-active');
        });
    });
});