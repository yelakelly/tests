function generateID(){
    return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
}

class Todo{
    constructor(text, status = false){
        this.text = text;
        this.status = status;
        this.id = `#${generateID()}`;
    }
    
    setActive(){
        this.status = true;
    }

    setDisable(){
        this.status = false;
    }

    render(){
        return `<div class="todo-list__item is-${this.status ? 'active' : 'disable'}" id="${this.id}">
            <input class="default-checkbox" type="checkbox" id='${this.id + '__label'}' ${this.status ? 'checked': ''}>
            <label for='${this.id + '__label'}'>
                <span>${this.text}</span>
                <span class="icon-tick-css"></span>
            </label>
            <span class="todo-list__remove-btn icon-close">&times;</span>
        </div>`
    }
}

class TodoList{
    constructor(selector){
      this.list = {};
      this.selector = selector;
      this.selector.$todo = this;
      
      this.setEvents();
    }

    add(el){
        this.list[el.id] = el;
        this.render();
    }

    getTodo(id){
        return this.list[id];
    }

    remove(id){
        delete this.list[id];
        this.render();
    }

    setEvents(){
        this.selector.addEventListener('click', (e) => {
            let el = e.target;

            if(el.classList.contains('todo-button')){
                this._createTodo()
            }

            if(el.classList.contains('todo-list__remove-btn')){
                let todoID = el.closest('.todo-list__item').getAttribute('id');
                this.remove(todoID);
            }
        });

        this.selector.addEventListener('keyup', (e) => {
            if(e.target.classList.contains('todo-input') && e.keyCode == 13){
                this._createTodo();
            }
        })

        this.selector.addEventListener('change', (e) => {
            let target = e.target;
            if(target.classList.contains('default-checkbox')){
                let todoID = target.getAttribute('id').replace('__label', '');
                let todo = this.getTodo(todoID);

                target.checked ? todo.setActive() : todo.setDisable();
                this.render();
            }
        }, {
            'capture': true
        })
    }

    render(){
        let markup = '';
        let values = Object.keys(this.list).map((e) => {
            return this.list[e]
        });

        values
            .forEach((el) => markup += el.render())

        this.selector.innerHTML  = `
            <input type="text" class="form-input todo-input" name="todo-input">
            <button class="todo-button btn">Добавить</button>
            <div class="todo-list">
                ${markup}
            </div>
        `;
    }

    validateInput(el){
        let valid = false;
        el.classList.remove('is-error');
        
        if(el.value.trim()){
            valid = true;
        }

        el.classList.add('is-error');
        return valid;
    }

    _createTodo(){
        let input = document.querySelector('input[name="todo-input"]');
                
        if(this.validateInput(input)){
            this.add(new Todo(input.value));
        }
    }
}

let react = new Todo('React');
let nodejs = new Todo('Node.js');

let todoList = new TodoList(document.querySelector('[todo-list]'));
todoList.add(react);
todoList.add(nodejs);