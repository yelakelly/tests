document.addEventListener("DOMContentLoaded", function(event) {
    let scroll = new SmoothScroll('a[href*="#"]');
    let moreButtons = document.querySelectorAll('.more-button');

    for(let i = 0; i < moreButtons.length; i++) {
        let btn = moreButtons[i];

        btn.addEventListener('click', function(e) {
            let target = document.querySelector(this.getAttribute('data-target'));

            target.classList.toggle('is-active');
            this.classList.toggle('is-open');

            let buttonInner = this.querySelector('span');

            if(this.classList.contains('is-open')){    
                buttonInner.innerHTML = buttonInner.getAttribute('data-hide-text');
            } else {
                buttonInner.innerHTML = buttonInner.getAttribute('data-show-text');
            }

            e.preventDefault();
        })
    }

    let classIndex = 0;
    let lastClass = 'has-tooltip--top';
    let classList = ['has-tooltip--right', 'has-tooltip--bottom', 'has-tooltip--left', 'has-tooltip--top'];

    document.querySelector('.has-tooltip').addEventListener('click', function() {
        this.classList.remove(lastClass);
        this.classList.add(classList[classIndex]);
        lastClass = classList[classIndex];
        classIndex = classIndex == classList.length - 1 ? 0 : classIndex + 1;
    });

    const copyToClipboard = () => {
        const el = document.createElement('textarea');
        el.value = Array
                .from(document.querySelectorAll('[to-clipboard]'))
                .reduce(function(prev, curr, index){
                    if(index == 1){
                        prev = prev.getAttribute('to-clipboard');
                    }
                    return prev + '\n' + curr.getAttribute('to-clipboard')
                });
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };

    document.querySelector('.about__buffer-control').addEventListener('click', () => {
        copyToClipboard();
    });

    document.querySelector('.js-nav-toggle').addEventListener('click', function(){
        this.classList.toggle('is-active');
        document.querySelector('body').classList.toggle('has-open-menu');
    });

    Array.from(document.querySelectorAll('.primary-nav__link')).forEach((el) => {
        el.addEventListener('click', function(){
            document.querySelector('body').classList.remove('has-open-menu');
            document.querySelector('.js-nav-toggle').classList.remove('is-active');
        });
    });
});